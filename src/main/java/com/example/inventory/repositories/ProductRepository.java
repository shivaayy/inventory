package com.example.inventory.repositories;

import com.example.inventory.model.ProductCollection;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductRepository extends MongoRepository<ProductCollection, Long> {

}
