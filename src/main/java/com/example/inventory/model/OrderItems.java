package com.example.inventory.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class OrderItems {
    private Long productId;
    private String productPrice;
    private int qty;


    @Override
    public String toString() {
        return "OrderItems{" +
                "productId=" + productId +
                ", productPrice='" + productPrice + '\'' +
                ", qty=" + qty +
                '}';
    }


}
