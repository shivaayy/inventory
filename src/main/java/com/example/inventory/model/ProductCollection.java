package com.example.inventory.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "product")
public class ProductCollection {
    @Id
    private long productId;
    private String productName;
    private int purchasePrice;
    private int sellPrice;
    private int availableQty;


    @Override
    public String toString() {
        return "ProductCollection{" +
                "productId=" + productId +
                ", productName='" + productName + '\'' +
                ", purchasePrice=" + purchasePrice +
                ", sellPrice=" + sellPrice +
                ", availableQty=" + availableQty +
                '}';
    }

}
