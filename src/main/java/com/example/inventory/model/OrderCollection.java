package com.example.inventory.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "order")
public class OrderCollection {

    @Id
    private Long orderId;
    private String orderType;
    public List<OrderItems> orderItems;

    @Override
    public String toString() {
        return "OrderCollection{" +
                "orderId=" + orderId +
                ", orderType='" + orderType + '\'' +
                ", orderItems=" + orderItems +
                '}';
    }

}
