package com.example.inventory.controllers;

import com.example.inventory.model.OrderCollection;
import com.example.inventory.model.OrderItems;
import com.example.inventory.model.ProductCollection;
import com.example.inventory.repositories.OrderRepository;
import com.example.inventory.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
public class OrderController {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ProductRepository productRepository;

    @PostMapping(value = "/order")
    public String placeOrder(@RequestBody OrderCollection orderCollections) {
        List<OrderItems> orderItems = orderCollections.getOrderItems();

        String message = "";
        OrderCollection newOrdercollection = new OrderCollection();
        newOrdercollection.setOrderId(orderCollections.getOrderId());
        newOrdercollection.setOrderType(orderCollections.getOrderType());
        newOrdercollection.setOrderItems(new ArrayList<>());
        for (OrderItems temp : orderCollections.getOrderItems()) {

            ProductCollection toChangeQty = productRepository.findById(temp.getProductId()).get();
            int currentQty = toChangeQty.getAvailableQty();
            int orderQty = temp.getQty();
            if (orderCollections.getOrderType().equals("purchase")) {
                newOrdercollection.orderItems.add(temp);
                toChangeQty.setAvailableQty(currentQty + orderQty);

            } else if (orderCollections.getOrderType().equals("sell")) {
                if (orderQty <= currentQty) {
                    newOrdercollection.orderItems.add(temp);
                    toChangeQty.setAvailableQty(currentQty - orderQty);
                } else {

                    message = message + "\nProduct Name:" + toChangeQty.getProductName() +
                            " ---- unable to place this item ( insufficient stock )";
                }

            }

            productRepository.save(toChangeQty);

        }
        orderRepository.insert(newOrdercollection);
        return "order placed" + message;

    }

    @GetMapping(value = "/orderdetails")
    public List<OrderCollection> getAllOrders() {
        return orderRepository.findAll();
    }
}
