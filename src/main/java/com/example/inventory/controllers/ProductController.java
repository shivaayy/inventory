package com.example.inventory.controllers;

import com.example.inventory.model.ProductCollection;
import com.example.inventory.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductController {

    @Autowired
    public ProductRepository productRepository;

    @GetMapping(value = "/product")
    public List<ProductCollection> getAllProducts() {

        return productRepository.findAll();
    }

    @PostMapping(value = "/addproduct")
    public String addProducts(@RequestBody List<ProductCollection> productCollection) {
        productRepository.insert(productCollection);

        return "Product added";
    }


}
